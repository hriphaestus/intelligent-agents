# Task 1

Requirements:
- Load a sample dataset from a CSV file
- Perform data manipulation (add/delete columns, merge dataframes, etc)
- Visualize the data with charts and plots